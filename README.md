# 基于微信小程序的共享车位系统的设计与实现

#### 介绍
基于微信小程序的共享车位系统的设计与实现系统实现（springboot+mybatis-plus+uni-app+vue）

#### 软件架构

前端开发使用 uni-app 框架技术开发打包成微信小程序，后端开发使用 SpringBoot 框架处理业务逻辑，用 Maven 管理后端项目依赖，数据业务层用 Mybatis-Plus 框架做数据库数据持久，进行与数据库交互，数据库使用 MySQL，后台管理前端用 Vue.js 框架加 ElementUI 组合作为表现层使用，通过这些流行的技术来进行系统开发。




#### 使用说明

1.  看文件中的部署文档
2.  附带相关说明文档

#### 系统相关截图
![输入图片说明](%E7%B3%BB%E7%BB%9F%E6%88%AA%E5%9B%BE/%E5%BE%AE%E4%BF%A1%E5%B0%8F%E7%A8%8B%E5%BA%8F/%E5%85%B1%E4%BA%AB%E8%BD%A6%E4%BD%8D%E5%B0%8F%E7%A8%8B%E5%BA%8F-%E7%AE%80%E4%BB%8B.png)
![输入图片说明](%E7%B3%BB%E7%BB%9F%E6%88%AA%E5%9B%BE/%E7%B3%BB%E7%BB%9F%E5%90%8E%E5%8F%B0%E7%AE%A1%E7%90%86/%E7%AE%A1%E7%90%86%E9%A6%96%E9%A1%B5.png)

![输入图片说明](%E7%B3%BB%E7%BB%9F%E6%88%AA%E5%9B%BE/%E7%B3%BB%E7%BB%9F%E5%90%8E%E5%8F%B0%E7%AE%A1%E7%90%86/%E5%85%B1%E4%BA%AB%E8%AE%A2%E5%8D%95%E7%AE%A1%E7%90%86.png)


